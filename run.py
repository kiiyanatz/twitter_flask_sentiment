from flask_script import Manager
from app_core import app, attach_extensions

manager = Manager(app)


@manager.command
def debug():
    ''' Starts application in debug mode '''

    app.config.from_object('config')
    attach_extensions(app)
    app.run()


@manager.command
def test():
    ''' Starts application in test mode '''

    app.config.from_pyfile('config.py')
    attach_extensions(app)
    app.run()


@manager.command
def prod():
    """ Start application in prodution mode """

    app.config.from_pyfile('config.py')
    attach_extensions(app)
    app.run()


if __name__ == "__main__":
    manager.run()
