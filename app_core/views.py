#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Contains all application routes.

Contains application routes and stuff
"""

from flask import Flask, render_template, session, request, json
from app_core import app
from app_core import twitter
from app_core import socketio, emit
from flask_json import json_response


@app.route('/')
def index():
    me = twitter.api.me()
    data = {'title': 'SIS Twitter Analytics',
            'header': 'SiS SuperTwitterBot'}
    return render_template('index.html', data=data, me=me)


@app.route('/tweets')
def tweets():
    timeline = twitter.api.home_timeline()

    for tweet in timeline:
        return json_response(id=tweet.id, tweet=tweet.text)


@socketio.on('my event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']})
