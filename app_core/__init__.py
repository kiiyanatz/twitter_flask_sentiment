from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from flask_json import FlaskJSON, JsonError, json_response, as_json

app = Flask(__name__, instance_relative_config=True)

# Create socketio instance
socketio = SocketIO()

# Create Flask_JSON instance
json = FlaskJSON()

def attach_extensions(app):
    socketio.init_app(app)
    json.init_app(app)

from app_core import views
from app_core import twitter
